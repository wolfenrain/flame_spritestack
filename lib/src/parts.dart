library flame_spritestack;

import 'dart:async';
import 'dart:math';

import 'package:flame/sprite_batch.dart';
import 'package:flame_spritestack/flame_spritestack.dart';
import 'package:flame_spritestack/src/sprites/layer_sprite.dart';
import 'package:flutter/widgets.dart';

export 'package:vector_math/vector_math_64.dart' show Vector2, Vector3;

// Sprites
part 'sprites/billboard_sprite.dart';
part 'sprites/sprite.dart';
part 'sprites/sprite_stack.dart';

// Utils
part 'camera2d.dart';
part 'sprite_stack_quality.dart';
part 'sprite_world.dart';
